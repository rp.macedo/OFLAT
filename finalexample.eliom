[%%shared
    open Eliom_lib
    open Eliom_content
    open Html.D
]

module Finalexample_app =
  Eliom_registration.App (
    struct
      let application_name = "finalexample"
      let global_data_path = None
    end)

let main_service =
  Eliom_service.create
    ~path:(Eliom_service.Path [])
    ~meth:(Eliom_service.Get Eliom_parameter.unit)
    ()

let script_uri =
  Eliom_content.Html.D.make_uri
      ~absolute:false   (* We want local file *)
      ~service:(Eliom_service.static_dir ())
      ["code8.js"]

let script_uri1 =
  Eliom_content.Html.D.make_uri
  (Eliom_service.extern
     ~prefix:"https://unpkg.com"
     ~path:["cytoscape";"dist"]
     ~meth:
       (Eliom_service.Get
          Eliom_parameter.(suffix (all_suffix "suff")))
     ())
    ["cytoscape.min.js"]

let script_uri2 =
  Eliom_content.Html.D.make_uri
      ~absolute:false   (* We want local file *)
      ~service:(Eliom_service.static_dir ())
      ["cytoscape-edgehandles"; "cytoscape-edgehandles.js"]

let script_uri3 =
  Eliom_content.Html.D.make_uri
  (Eliom_service.extern
     ~prefix:"https://unpkg.com"
     ~path:["popper.js@1.14.7";"dist"; "umd"]
     ~meth:
       (Eliom_service.Get
          Eliom_parameter.(suffix (all_suffix "suff")))
     ())
    ["popper.js"]

let script_uri4 =
  Eliom_content.Html.D.make_uri
      ~absolute:false   (* We want local file *)
      ~service:(Eliom_service.static_dir ())
      ["cytoscape-popper"; "cytoscape-popper.js"]

let script_uri5 =
  Eliom_content.Html.D.make_uri
  (Eliom_service.extern
     ~prefix:"https://cdnjs.cloudflare.com"
     ~path:["ajax"; "libs"; "lodash.js"; "4.17.10"]
     ~meth:
       (Eliom_service.Get
          Eliom_parameter.(suffix (all_suffix "suff")))
     ())
    ["lodash.js"]

let script_uri6 =
  Eliom_content.Html.D.make_uri
  (Eliom_service.extern
     ~prefix:"https://unpkg.com"
     ~path:["tippy.js@4.0.1"; "umd"]
     ~meth:
       (Eliom_service.Get
          Eliom_parameter.(suffix (all_suffix "suff")))
     ())
    ["index.all.min.js"]

[%%client
let js_eval s =
	Js_of_ocaml.Js.Unsafe.eval_string s

let js_run s =
	ignore (js_eval s)

let js_console =
	Js_of_ocaml.Firebug.console

let js_string s =
	Js_of_ocaml.Js.string s

let js_log s =
	ignore (js_console##log (js_string s))

type symbol = char     (* our symbols are represented by chars *)
type word = symbol list  (* our words are represented by lists of symbols *)

type state = string      (* our states are represented by strings *)
type states = state list

type transition =
    state   (* state *)
  * symbol  (* consumed input symbol *)
  * state   (* next state *)

type transitions = transition list

type fAutomaton = {
    initialState: state;       (* Initial state *)
    transitions: transitions;  (* Transition relation *)
    acceptStates: states       (* Accept states *)
}

let abc = {
    initialState = "START" ;
    transitions = [
            ("START",'a',"A"); ("START",'b',"START"); ("START",'c',"START");
                                                      ("START",'d',"START");
            ("A",'a',"A"); ("A",'b',"AB"); ("A",'c',"START"); ("A",'d',"START"); 
            ("AB",'a',"START"); ("AB",'b',"START"); ("AB",'c',"SUCCESS");
                                                    ("AB",'d',"START");
            ("SUCCESS",'a',"SUCCESS"); ("SUCCESS",'b',"SUCCESS");
                         ("SUCCESS",'c',"SUCCESS"); ("SUCCESS",'d',"SUCCESS")
        ];
    acceptStates = ["SUCCESS"]
}

let example1NFA = {
      initialState = "START";
      transitions = [
        ("START", 'a', "A"); ("A", 'b', "B"); ("A", 'b', "START"); ("B", 'a', "START")

      ];
      acceptStates = ["START"]
}

let example1DFA = {
      initialState = "START";
      transitions = [
        ("START", 'a', "A"); ("A", 'b', "B"); ("B", 'a', "C"); ("C", 'b', "B"); ("C", 'a', "A")
      ];
      acceptStates = ["START"; "B"; "C"]
}

let abcND = {
    initialState = abc.initialState ;
    transitions = abc.transitions @ [
			("SUCCESS",'a',"SUCCESS");
            ("UNREACHABLE",'a',"SUCCESS");
            ("SUCCESS",'e',"UNPRODUTIVE"); ("UNPRODUTIVE",'a',"UNPRODUTIVE")
        ];
    acceptStates = abc.acceptStates
}

let sentence : symbol list ref = ref []

let automata = ref {
  initialState = "";
  transitions = [];
  acceptStates = []
}

let newNode (c1, c2, c3) = {
            initialState = !automata.initialState;
            transitions = !automata.transitions @ [
                (c1,c2,c3)
            ];
            acceptStates = !automata.acceptStates
}

let newNodeFinal final = {
            initialState = !automata.initialState;
            transitions = !automata.transitions;
            acceptStates = !automata.acceptStates @ [final]
}

let step : string ref = ref "START"

let setStep a = a

let position : int ref = ref 0

let listStates: string list ref = ref []

let newSentence : string ref = ref ""

let last1 node = List.mem node !automata.acceptStates

let createNode (f, s, t) =
  let test = last1 f in 
  js_run ("makeNode1('" ^ f ^ "', '" ^ string_of_bool (test) ^ "')");
  let test = last1 t in 
  js_run ("makeNode1('" ^ t ^ "', '" ^ string_of_bool (test) ^ "')")

let createEdge (f, s, t) = 
  js_run ("makeEdge('" ^ f ^ "', '" ^ t ^ "', '" ^ (String.make 1 s) ^ "')");
  js_log (String.make 1 s)

let rec findtransitions (trans) =
  match trans with 
    [] -> false 
    | x::xs -> createNode(x); createEdge (x); findtransitions (xs)

  let inputNodes (gra) =
  automata := gra;
  js_run ("start()");
  ignore (findtransitions (gra.transitions))

let canonical l =
    List.sort_uniq compare l

let getStates fa =
	canonical (
		fa.initialState
		:: List.map (fun (s,_,_) -> s) fa.transitions
		@ List.map (fun (_,_,s) -> s) fa.transitions
		@ fa.acceptStates
	)

let transitionsFor (s,symb) fa =
	List.filter (fun (s1,symb1,_) -> (s,symb) = (s1,symb1)) fa.transitions

let transitionsForBack (symb, s) fa =
  js_log "entra aqui";
	List.filter (fun (_,symb1,s1) -> (symb,s) = (symb1,s1)) fa.transitions

let rec delay1 n = if n = 0 then () else delay1 (n-1)

let rec delay  n = if n = 0 then 
                      Lwt.return ()
                    else Lwt.bind (Lwt.pause ()) (fun () -> delay (n-1))

let rec acceptX s w fa =
  let last = last1 s in 
  js_run ("changeColor1('" ^ s ^ "', '" ^ (string_of_int (List.length w)) ^ "', '" ^ string_of_bool (last) ^ "')");
	match w with
		  [] -> 
			  Lwt.return (List.mem s fa.acceptStates)
		| x::xs ->
			match transitionsFor (s,x) fa with
					[] -> Lwt.return (js_run ("changeColor1('" ^ s ^ "', '" ^ (string_of_int (0)) ^ "', '" ^ string_of_bool (false) ^ "')"); false)
				 | (_,_,s)::_ -> 
            Lwt.bind (delay 50)
            (fun () ->
              Lwt.bind (Lwt.return (let last3 = last1 s in js_run ("changeColor1('" ^ s ^ "', '" ^ (string_of_int (List.length xs)) ^ "', '" ^ string_of_bool (last3) ^ "')"))) 
                        (fun () -> acceptX s xs fa))

let accept w fa =
	acceptX fa.initialState w fa

let rec get_nth mylist index = match mylist with
    | [] -> raise (Failure "empty list")
    | first::rest -> 
        if index = 0 then first 
        else get_nth rest (index-1)

let reduceSentence () = 
  newSentence := "";
  for i = !position to (List.length !sentence) - 1 do 
    newSentence := !newSentence ^ String.make 1 (List.nth !sentence i);
  done

let acceptStep fa =
  if (!position == List.length !sentence) then (false)
  else
  (let x = get_nth !sentence !position in 
	match transitionsFor (!step,x) fa with
			[] -> reduceSentence(); 
            js_run ("changeColor1('" ^ !step ^ "', '" ^ string_of_int (0) ^ "', '" ^ string_of_bool (false) ^ "')"); false
			| (_,_,s)::_ -> step := setStep s; reduceSentence();
                      position := !position + 1; 
                      let last = List.length !sentence - !position in 
                      let test = last1 s in
                      js_run ("changeColor1('" ^ s ^ "', '" ^ string_of_int (last) ^ "', '" ^ string_of_bool (test) ^ "')"); 
                      true)

let acceptStepBack w =
  if (!position > 0) then 
    (position := !position - 1; 
    let st = get_nth !listStates !position in 
    let last = List.length w - !position in 
    let test = last1 st in 
    js_run ("changeColor1('" ^ st ^ "', '" ^ string_of_int (last) ^ "', '" ^ string_of_bool (test) ^ "')");
    step := setStep st)

let rec listOfStates s w fa =
	match w with
		  [] -> false
		| x::xs ->
			match transitionsFor (s,x) fa with
					[] -> false
				 | (_,_,s)::_ -> listStates := !listStates @ [s]; listOfStates s xs fa; false

let start w fa =
  listStates := [];
  position := 0;
  step := setStep fa.initialState;
  listStates := !listStates @ [!step];
  ignore (listOfStates !step w fa);
  let last = List.length w in
  let test = last1 !step in
  js_run ("changeColor1('" ^ !step ^ "', '" ^ string_of_int (last) ^ "', '" ^ string_of_bool (test) ^ "')")

let flatMap f l =
    List.flatten (List.map f l)

let gcut s ts =
	List.partition (fun (a,b,c) -> a = s) ts

let rec reachableX s ts =
	let (x, xs) = gcut s ts in
		s::flatMap (fun (_,_,s) -> reachableX s xs) x

let rec intersection l1 l2 =
     match l1 with
        [] -> []
      | x::xs -> (if List.mem x l2 then [x] else []) @ intersection xs l2

let productive fa =
    let allStates = getStates fa in
		let reachAccepted s = intersection fa.acceptStates (reachableX s fa.transitions) <> [] in
			List.filter reachAccepted allStates

let reachable fa = canonical (reachableX fa.initialState fa.transitions)

let paintP f = js_run ("paintProd('" ^ f ^ "')")

let paintR f = js_run ("paintReach('" ^ f ^ "')")

let paintProductive fa =
  js_run ("resetStyle()");
  List.iter paintP (productive fa)

let paintReachable fa =
  js_run ("resetStyle()");
  List.iter paintR (reachable fa)

let rec notIntersection l1 l2 =
     match l1 with
        [] -> []
      | x::xs -> (if List.mem x l2 then [] else [x]) @ notIntersection xs l2

let paintUse f = js_run ("paintUseful('" ^ f ^ "')")

let paintUseful l1 l2 =
  js_run ("resetStyle()");
  let list = intersection l1 l2 in 
  List.iter paintUse list
] 

let upload =
  let onclick_handler5 = [%client (fun _ ->
    js_run ("destroy1()");
    inputNodes (example1DFA)
  )] in
  let button5 = button ~a:[a_id "invisible"; a_onclick onclick_handler5] [txt "Example 1"] in
  let onclick_handler6 = [%client (fun _ ->
    js_run ("destroy1()");
    inputNodes (example1NFA)
  )] in
  let button6 = button ~a:[a_id "invisible"; a_onclick onclick_handler6] [txt "Example 1"] in
  let onclick_handler7 = [%client (fun _ ->
    js_run ("destroy1()");
    inputNodes (abc)
  )] in
  let button7 = button ~a:[a_id "invisible"; a_onclick onclick_handler7] [txt "Example 2"] in
  let onclick_handler8 = [%client (fun _ ->
    js_run ("destroy1()");
    inputNodes (abcND)
  )] in
  let button8 = button ~a:[a_id "invisible"; a_onclick onclick_handler8] [txt "Example 2"] in
  div ~a:[a_id "examples"] [div ~a:[a_id "leftBox"] [txt "Exemplos de AFD"; button5; button7]; 
                                           div ~a:[a_id "rightBox"] [txt "Exemplos de AFN"; button6; button8]]

let generate =
  let input1 = input ~a:[a_id "box"; a_input_type `Text]() in
  let input2 = input ~a:[a_id "box"; a_input_type `Text]() in
  let input3 = input ~a:[a_id "box"; a_input_type `Text]() in
  let input4 = input ~a:[a_id "box"; a_input_type `Text]() in
  let onclick_handler2 = [%client (fun _ ->
    let i = (Eliom_content.Html.To_dom.of_input ~%input1) in
    let v = Js_of_ocaml.Js.to_string i##.value in
    js_run ("makeNode1('" ^ v ^ "', '" ^ string_of_bool (false) ^ "')")
  )] in
  let button2 = button ~a:[a_onclick onclick_handler2] [txt "Adicionar estado"] in
  let onclick_handler3 = [%client (fun _ ->
    let i1 = (Eliom_content.Html.To_dom.of_input ~%input2) in
    let i2 = (Eliom_content.Html.To_dom.of_input ~%input3) in
    let i3 = (Eliom_content.Html.To_dom.of_input ~%input4) in
      let v1 = Js_of_ocaml.Js.to_string i1##.value in
      let v2 = Js_of_ocaml.Js.to_string i2##.value in
      let v3 = Js_of_ocaml.Js.to_string i3##.value in
        let c3 = String.get v3 0 in 
          ignore (automata := newNode (v1, c3, v2));
          js_run ("makeEdge('" ^ v1 ^ "', '" ^ v2 ^ "', '" ^v3^"')");
  )] in
  let button3 = button ~a:[a_onclick onclick_handler3] [txt "Adicionar Transição"] in
  let onclick_handler4 = [%client (fun _ ->
    automata := {
      initialState = "START";
      transitions = [];
      acceptStates = []
    };
    js_run ("destroy1()");
    js_run ("start()");
    js_run ("makeNode1('" ^ "START" ^ "', '" ^ string_of_bool (false) ^ "')");
  )] in 
  let button4 = button ~a:[a_onclick onclick_handler4] [txt "Adicionar estado inicial"] in
  let onclick_handler5 = [%client (fun _ ->
    automata := {
      initialState = "START";
      transitions = [];
      acceptStates = ["START"]
    };
    js_run ("destroy1()");
    js_run ("start()");
    js_run ("makeNode1('" ^ "START" ^ "', '" ^ string_of_bool (true) ^ "')");
    automata := newNodeFinal "START"
  )] in 
  let button5 = button ~a:[a_onclick onclick_handler5] [txt "Adicionar estado inicial como final"] in
  let onclick_handler6 = [%client (fun _ ->
    let i = (Eliom_content.Html.To_dom.of_input ~%input1) in
    let v = Js_of_ocaml.Js.to_string i##.value in
    js_run ("makeNode1('" ^ v ^ "', '" ^ string_of_bool (true) ^ "')");
    automata := newNodeFinal v
  )] in
  let button6 = button ~a:[a_onclick onclick_handler6] [txt "Adicionar estado final"] in
  div ~a:[a_class ["mywidget"]] [button4; button5; div ~a:[a_id "space"][txt "Nome: ";input1; button2; button6]; div [txt "Estado de partida: "; input2; txt " Estado de chegada: "; input3; txt " Transição: "; input4; button3]]

let%client switch_visibility elt =
  let elt = Eliom_content.Html.To_dom.of_element elt in
  if Js_of_ocaml.Js.to_bool (elt##.classList##(contains (Js_of_ocaml.Js.string "hidden"))) then
    elt##.classList##remove (Js_of_ocaml.Js.string "hidden")
  else
    elt##.classList##add (Js_of_ocaml.Js.string "hidden")

let mywidget s1 s2 =
  let button1  = div ~a:[a_class ["button"]] [h3 ~a: [a_id "under"] [txt s1]] in
  let content = div ~a:[a_class ["content"; "hidden"]] [s2] in
  let _ = [%client
    (Lwt.async (fun () ->
       Lwt_js_events.clicks (Eliom_content.Html.To_dom.of_element ~%button1)
         (fun _ _ -> switch_visibility ~%content; Lwt.return ()))
     : unit)
  ] in
  div ~a:[a_class ["mywidget"]] [button1; content]

let verify = 
  let input0 = input ~a:[a_input_type `Text]() in
      let onclick_handler1 = [%client (fun _ ->
        let i = (Eliom_content.Html.To_dom.of_input ~%input0) in
        let v = Js_of_ocaml.Js.to_string i##.value in
        sentence := List.init (String.length v) (fun k -> String.get v k);
        ignore (accept !sentence !automata);
      )         
      ] in
      let button1 = button ~a:[a_onclick onclick_handler1] [txt "Testar frase completa"] in
      let onclick_handler5 = [%client (fun _ ->
        let i = (Eliom_content.Html.To_dom.of_input ~%input0) in
        let v = Js_of_ocaml.Js.to_string i##.value in
        sentence := List.init (String.length v) (fun k -> String.get v k);
        ignore (start !sentence !automata);
      )         
      ] in
      let button5 = button ~a:[a_onclick onclick_handler5] [txt "Começar passo a passo"]
      in
      let onclick_handler4 = [%client (fun _ ->
          ignore (acceptStep !automata);
          reduceSentence ();
          let i0 = (Eliom_content.Html.To_dom.of_input ~%input0) in
          i0##.value:= Js_of_ocaml.Js.string !newSentence
      )
      ] in
      let button4 = button ~a:[a_onclick onclick_handler4] [txt "Avançar"]
      in
       let onclick_handler6 = [%client (fun _ ->
          ignore (acceptStepBack !sentence);
          reduceSentence ();
          let i0 = (Eliom_content.Html.To_dom.of_input ~%input0) in
          i0##.value:= Js_of_ocaml.Js.string !newSentence
      )
      ] in
      let button6 = button ~a:[a_onclick onclick_handler6] [txt "Retroceder"]
      in 
      div ~a:[a_class ["mywidget"]] [txt "Palavra: "; input0; button1; button5; button6; button4]

let evaluate = 
  let onclick_handler9 = [%client (fun _ ->
          paintProductive !automata
      )] in
      let button9 = button ~a:[a_onclick onclick_handler9] [txt "Estados Produtivos"]
      in
      let onclick_handler10 = [%client (fun _ ->
          paintReachable !automata
      )] in
      let button10 = button ~a:[a_onclick onclick_handler10] [txt "Estados Acessíveis"]
      in
      let onclick_handler11 = [%client (fun _ ->
          paintUseful (reachable !automata) (productive !automata)
      )] in
      let button11 = button ~a:[a_onclick onclick_handler11] [txt "Estados Úteis"]
      in
      div ~a:[a_class ["mywidget"]] [button9; button10; button11;]
                                      
let hiddenBox2 test = div ~a:[a_id "examples"] [test]

let () =
  Finalexample_app.register
    ~service:main_service
    (fun () () ->
      let open Eliom_content.Html.D in
       Lwt.return
         (html
            (head (title (txt "Autómatos Animados")) [script ~a:[a_src script_uri1] (txt ""); 
                                        script ~a:[a_src script_uri3] (txt "");
                                        script ~a:[a_src script_uri5] (txt "");
                                        script ~a:[a_src script_uri4] (txt "");
                                        script ~a:[a_src script_uri6] (txt "");
                                          script ~a:[a_src script_uri2] (txt ""); 
                                          css_link ~uri: (make_uri (Eliom_service.static_dir ()) ["codecss2.css"]) ();
                                          script ~a:[a_src script_uri] (txt "");
                                          ])
            (body [ div [h1 [txt "Autómatos Animados"]];
                    div ~a:[a_id "inputBox"] 
                           [h2 [txt "Ações"];
                            mywidget "Carregar Autómatos" (hiddenBox2 upload);
                            mywidget "Gerar Autómato" (hiddenBox2 generate);
                            mywidget "Testar aceitação de palavra" (hiddenBox2 verify);
                            mywidget "Avaliar natureza dos estados" (hiddenBox2 evaluate);
                           ];
                    div ~a:[a_id "cy"] [];
                    ] 
            )
          )    
    )    
                